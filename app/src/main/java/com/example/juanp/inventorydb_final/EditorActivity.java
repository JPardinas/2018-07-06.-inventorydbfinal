package com.example.juanp.inventorydb_final;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.juanp.inventorydb_final.data.InventoryContract.InventoryEntry;

public class EditorActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    //Inventory loader and uri
    private static final int EXISTING_INVENTORY_LOADER = 0;
    private Uri currentProductUri;

    //Declaration of the editTexts
    private EditText productNameEditText;
    private EditText productPriceEditText;
    private EditText productQuantityEditText;
    private Spinner productSupplierNameSpinner;
    private EditText productSupplierPhoneNumberEditText;

    //Supplier name
    private int supplierName = InventoryEntry.SUPPLIER_ANOTHER;

    //Boolean product change
    private boolean productHasChanged = false;

    //Touch listener
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            productHasChanged = true;
            Log.d("message", "onTouch");

            return false;
        }
    };

    //Oncreate method
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        Log.d("message", "onCreate");

        //Intent
        Intent intent = getIntent();
        currentProductUri = intent.getData();

        //If else to change title
        if (currentProductUri == null) {
            setTitle(getString(R.string.add_product));
            invalidateOptionsMenu();
        } else {
            setTitle(getString(R.string.edit_product));
            getLoaderManager().initLoader(EXISTING_INVENTORY_LOADER, null, this);
        }

        productNameEditText = findViewById(R.id.product_name_edit_text);
        productPriceEditText = findViewById(R.id.product_price_edit_text);
        productQuantityEditText = findViewById(R.id.product_quantity_edit_text);
        productSupplierNameSpinner = findViewById(R.id.product_supplier_name_spinner);
        productSupplierPhoneNumberEditText = findViewById(R.id.product_supplier_phone_number_edit_text);

        productNameEditText.setOnTouchListener(touchListener);
        productPriceEditText.setOnTouchListener(touchListener);
        productQuantityEditText.setOnTouchListener(touchListener);
        productSupplierNameSpinner.setOnTouchListener(touchListener);
        productSupplierPhoneNumberEditText.setOnTouchListener(touchListener);

        //Call set up spinner method
        setupSpinner();
    }

    //On Create options menu method
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        Log.d("message", "open Editor Activity");
        return true;
    }

    //On Options item selected method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                insertItem();
                return true;
            case android.R.id.home:
                if (!productHasChanged) {
                    NavUtils.navigateUpFromSameTask(EditorActivity.this);
                    return true;
                }
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(EditorActivity.this);
                            }
                        };
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!productHasChanged) {
            super.onBackPressed();
            return;
        }
        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                };

        showUnsavedChangesDialog(discardButtonClickListener);
    }

    //Loader
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                InventoryEntry._ID,
                InventoryEntry.COLUMN_PRODUCT_NAME,
                InventoryEntry.COLUMN_PRICE,
                InventoryEntry.COLUMN_QUANTITY,
                InventoryEntry.COLUMN_SUPPLIER_NAME,
                InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER
        };
        return new CursorLoader(this,
                currentProductUri,
                projection,
                null,
                null,
                null);
    }

    //Loader finish method
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor == null || cursor.getCount() < 1) {
            return;
        }
        if (cursor.moveToFirst()) {
            //Get columns
            int nameColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_PRODUCT_NAME);
            int priceColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_PRICE);
            int quantityColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_QUANTITY);
            int supplierNameColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_SUPPLIER_NAME);
            int supplierPhoneColumnIndex = cursor.getColumnIndex(InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER);

            String currentName = cursor.getString(nameColumnIndex);
            int currentPrice = cursor.getInt(priceColumnIndex);
            int currentQuantity = cursor.getInt(quantityColumnIndex);
            int currentSupplierName = cursor.getInt(supplierNameColumnIndex);
            int currentSupplierPhone = cursor.getInt(supplierPhoneColumnIndex);

            //Setting texts
            productNameEditText.setText(currentName);
            productPriceEditText.setText(Integer.toString(currentPrice));
            productQuantityEditText.setText(Integer.toString(currentQuantity));
            productSupplierPhoneNumberEditText.setText(Integer.toString(currentSupplierPhone));

            //Swith with the supplier name
            switch (currentSupplierName) {
                case InventoryEntry.SUPPLIER_MEDIAMARKT:
                    productSupplierNameSpinner.setSelection(1);
                    break;
                case InventoryEntry.SUPPLIER_AMAZON:
                    productSupplierNameSpinner.setSelection(2);
                    break;
                case InventoryEntry.SUPPLIER_PCCOMPONENTES:
                    productSupplierNameSpinner.setSelection(3);
                    break;
                default:
                    productSupplierNameSpinner.setSelection(0);
                    break;
            }
        }
    }

    //Loader reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        productNameEditText.setText("");
        productPriceEditText.setText("");
        productQuantityEditText.setText("");
        productSupplierPhoneNumberEditText.setText("");
        productSupplierNameSpinner.setSelection(0);
    }

    //Set up spinner method
    private void setupSpinner() {

        //Array adapter of the spinner
        ArrayAdapter productSupplieNameSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.array_suppliers, android.R.layout.simple_spinner_item);

        //View resource, set adapter and listener
        productSupplieNameSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        productSupplierNameSpinner.setAdapter(productSupplieNameSpinnerAdapter);
        productSupplierNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            //Sets the supplier
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.supplier_mediamarkt))) {
                        supplierName = InventoryEntry.SUPPLIER_MEDIAMARKT;
                    } else if (selection.equals(getString(R.string.supplier_amazon))) {
                        supplierName = InventoryEntry.SUPPLIER_AMAZON;
                    } else if (selection.equals(getString(R.string.supplier_pccomponentes))) {
                        supplierName = InventoryEntry.SUPPLIER_PCCOMPONENTES;
                    } else {
                        supplierName = InventoryEntry.SUPPLIER_ANOTHER;
                    }
                }
            }

            //Default supplier
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                supplierName = InventoryEntry.SUPPLIER_ANOTHER;
            }
        });
    }


    private void insertItem() {

        //Get the input information
        String productNameString = productNameEditText.getText().toString().trim();
        String productPriceString = productPriceEditText.getText().toString().trim();
        String productQuantityString = productQuantityEditText.getText().toString().trim();
        String productSupplierPhoneNumberString = productSupplierPhoneNumberEditText.getText().toString().trim();

        //If else to require the fields
        if (currentProductUri == null) {
            if (TextUtils.isEmpty(productNameString)) {
                Toast.makeText(this, getString(R.string.product_name_requires), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productPriceString)) {
                Toast.makeText(this, getString(R.string.price_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productQuantityString)) {
                Toast.makeText(this, getString(R.string.quantity_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (supplierName == InventoryEntry.SUPPLIER_ANOTHER) {
                Toast.makeText(this, getString(R.string.supplier_name_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productSupplierPhoneNumberString)) {
                Toast.makeText(this, getString(R.string.supplier_phone_required), Toast.LENGTH_SHORT).show();
                return;
            }

            //Insert method
            //Insert the values
            ContentValues values = new ContentValues();

            values.put(InventoryEntry.COLUMN_PRODUCT_NAME, productNameString);
            values.put(InventoryEntry.COLUMN_PRICE, productPriceString);
            values.put(InventoryEntry.COLUMN_QUANTITY, productQuantityString);
            values.put(InventoryEntry.COLUMN_SUPPLIER_NAME, supplierName);
            values.put(InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER, productSupplierPhoneNumberString);

            Uri newUri = getContentResolver().insert(InventoryEntry.CONTENT_URI, values);

            //If else if insert fail or not, and to require fields, with toast
            if (newUri == null) {
                Toast.makeText(this, getString(R.string.insert_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.insert_successful),
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {

            if (TextUtils.isEmpty(productNameString)) {
                Toast.makeText(this, getString(R.string.product_name_requires), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productPriceString)) {
                Toast.makeText(this, getString(R.string.price_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productQuantityString)) {
                Toast.makeText(this, getString(R.string.quantity_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (supplierName == InventoryEntry.SUPPLIER_ANOTHER) {
                Toast.makeText(this, getString(R.string.supplier_name_required), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(productSupplierPhoneNumberString)) {
                Toast.makeText(this, getString(R.string.supplier_phone_required), Toast.LENGTH_SHORT).show();
                return;
            }

            //Editing method
            //Insert the values
            ContentValues values = new ContentValues();

            values.put(InventoryEntry.COLUMN_PRODUCT_NAME, productNameString);
            values.put(InventoryEntry.COLUMN_PRICE, productPriceString);
            values.put(InventoryEntry.COLUMN_QUANTITY, productQuantityString);
            values.put(InventoryEntry.COLUMN_SUPPLIER_NAME, supplierName);
            values.put(InventoryEntry.COLUMN_SUPPLIER_PHONE_NUMBER, productSupplierPhoneNumberString);


            int rowsAffected = getContentResolver().update(currentProductUri, values, null, null);
            if (rowsAffected == 0) {
                Toast.makeText(this, getString(R.string.update_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.update_successful),
                        Toast.LENGTH_SHORT).show();
                finish();
            }

        }
    }

    //Method to advice user before back
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
