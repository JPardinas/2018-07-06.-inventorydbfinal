package com.example.juanp.inventorydb_final.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class InventoryContract {

    //Content, uri and path
    public static final String CONTENT_AUTHORITY = "com.example.juanp.inventorydb_final";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_INVENTORY = "inventory";

    public InventoryContract() {
    }

    public final static class InventoryEntry implements BaseColumns {

        //Table name
        public final static String TABLE_NAME = "inventory";
        //ID
        public final static String _ID = BaseColumns._ID;
        //Name
        public final static String COLUMN_PRODUCT_NAME = "product_name";
        //Price
        public final static String COLUMN_PRICE = "price";
        //Quantity
        public final static String COLUMN_QUANTITY = "quantity";
        //Supplier name
        public final static String COLUMN_SUPPLIER_NAME = "supplier_name";
        //Supplier phone number
        public final static String COLUMN_SUPPLIER_PHONE_NUMBER = "supplier_phone_number";

        //Uri
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_INVENTORY);
        //Types
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INVENTORY;

        //Suppliers
        public final static int SUPPLIER_ANOTHER = 0;
        public final static int SUPPLIER_MEDIAMARKT = 1;
        public final static int SUPPLIER_AMAZON = 2;
        public final static int SUPPLIER_PCCOMPONENTES = 3;

        //Boolean to check if is a valid supplier name
        public static boolean isValidSupplierName(int suppliername) {
            if (suppliername == SUPPLIER_ANOTHER || suppliername == SUPPLIER_MEDIAMARKT || suppliername == SUPPLIER_AMAZON || suppliername == SUPPLIER_PCCOMPONENTES) {
                return true;
            }
            return false;
        }
    }
}
