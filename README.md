Project Overview

You've made it to the end! This is the final part of project where you'll create a single amazing app that will pull together 
many of the components you've learned in this Nanodegree program!

The goal is to design and create the structure of an Inventory App which would allow a store to keep track of its inventory. 
In Stage 1, you designed and implemented the backend database that stores information about the products and suppliers. 
In this stage, you'll build out the app UI for users to add and remove inventory.